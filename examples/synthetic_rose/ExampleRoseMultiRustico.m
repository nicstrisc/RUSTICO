addpath('../../src/RUSTICO/');
addpath('../../src/utils/');

% Example code how to configure and combine multiple RUSTICOs 

%%
% [1] Nicola Strisciuglio, George Azzopardi, Nicolai Petkov, 'Robust 
%     Inhibition-augmented operator for delineation of curvilinear 
%     structures', IEEE Transactions on Image Processing, 2019
%

%% Load image and ground truth
I = imread('IM_plant0000_L.png');
I = rgb2gray(I); 
I = double(I) ./ 255;
I = imcomplement(I);
GT = imread('seg0000_L.png');
GT = rgb2gray(GT);
GT = double(GT) ./ 255;

%% Parameters (see [1] for details)
% The paramenters are configured for a set of RUSTICO filters
rho = cell(1);
sigma = cell(1);
sigma0 = cell(1);
alpha = cell(1);
xi = cell(1);
lambda = cell(1);

% RUSTICO filter 1
rho{1} = 25;
sigma{1} = [3 5 7]; %[4, 12, 20, 28];
sigma0{1} = 2;
alpha{1} = 0.8;
xi{1} = 1.5;         
lambda{1} = 2;

% RUSTICO filter 2
rho{2} = 30;
sigma{2} = [4 8]; %[4, 12, 20, 28];
sigma0{2} = 2.5;
alpha{2} = 0.5;
xi{2} = 1.5;         
lambda{2} = 2;

% number of configured RUSTICOs
nfilters = numel(rho);

numoriens = 8;

%% Configuration of RUSTICO (multiple filters)
x = 101; y = 101; % center
line1(:, :) = zeros(201);
line1(:, x) = 1; %prototype line

filterset = cell(1);
for n = 1:nfilters
    rustico = cell(1);
    params = config_params;

    % Basic COSFIRE parameters
    params.inputfilter.DoG.sigmalist    = sigma{n};
    params.COSFIRE.rholist              = 0:2:rho{n};
    params.COSFIRE.sigma0               = sigma0{n} / 6;
    params.COSFIRE.alpha                = alpha{n} / 6;
    % Inhibition
    params.RUSTICO.xi                   = xi{n};       % Inihbition factor
    params.RUSTICO.lambda               = lambda{n};   % Standard deviation factor

    % Orientations
    params.invariance.rotation.psilist = 0:pi/numoriens:pi-pi/numoriens;

    % Configuration
    filterset{n} = configureRUSTICO(line1, round([y x]), params);
end

%% Application of RUSTICO 
% Pad input image to avoid border effects
NP = 0; 
I = padarray(I, [NP NP], 'replicate');

% Compute the responses (and rotation information) for all the RUSTICO
% filters in the set
resp = zeros(size(I, 1), size(I, 2), nfilters);
rot = zeros(size(I, 1), size(I, 2), numoriens, nfilters);
for n = 1:nfilters
    tuple = computeTuples(I, filterset{n});
    [response, rotations] = applyRUSTICO(I, filterset{n}, params.RUSTICO.xi, tuple);
    response = response{1};
    resp(:, :, n) = response(NP+1:end-NP, NP+1:end-NP);
    % Save the orientation responses (use of the orientation map)
    rotations_final = zeros(size(response, 1), size(response, 2), size(rotations, 3));
    for j = 1:numoriens %size(rotations, 3)
        rotations_final(:, :, j) = rotations(NP+1:end-NP, NP+1:end-NP, j);
    end
    rot(:, :, :, n) = rotations_final;
end

% Combination of the responses of the RUSTICO filters
[response, i_max_rustico] = max(resp, [], 3);
rotations_final = zeros(size(response, 1), size(response, 2), numoriens);
for n = 1:nfilters
    r = rot(:, :, :, n);
    indmat = (i_max_rustico == n);
    rotations_final = rotations_final + r .* repmat(indmat, [1, 1, numoriens]);
end

% -------------------------------------------------------------------------



% Evaluation
perf_threshold = 49;
thresholds = 0.01:0.01:0.99;
%subplot(1,2,1); imagesc(response); colormap gray; axis off; axis image;
for j = 1:numel(thresholds)
    binImg = binarize(rotations_final, thresholds(j));
    binImg2 = bwmorph(binImg, 'close');
    %subplot(1,2,2); 
    imshow(binImg2);
    drawnow;
    res = evalperf_1pxline(binImg2, GT, 0, 2);
    RESULTS(j, :) = [res.Precision, res.Recall, res.FMeasure];
    binImg = [];
    binImg2 = [];
end


