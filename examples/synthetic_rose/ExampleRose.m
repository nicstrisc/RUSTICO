addpath('../../src/RUSTICO/');
addpath('../../src/utils/');

%%
% [1] Nicola Strisciuglio, George Azzopardi, Nicolai Petkov, 'Robust 
%     Inhibition-augmented operator for delineation of curvilinear 
%     structures', IEEE Transactions on Image Processing, 2019
%

%% Load image and ground truth
I = imread('IM_plant0000_L.png');
I = rgb2gray(I);
I = double(I) ./ 255;
I = imcomplement(I);
GT = imread('seg0000_L.png');
GT = rgb2gray(GT);
GT = double(GT) ./ 255;

%% Parameters (see [1] for details)
rho = 30;
sigma = [3 5 7]; %[4, 12, 20, 28];
sigma0 = 2;
alpha = 0.8;
xi = 1.5;         
lambda = 2;

numoriens = 8;

%% Configuration of RUSTICO
x = 101; y = 101; % center
line1(:, :) = zeros(201);
line1(:, x) = 1; %prototype line

rustico = cell(1);
params = config_params;

% Basic COSFIRE parameters
params.inputfilter.DoG.sigmalist    = sigma;
params.COSFIRE.rholist              = 0:2:rho;
params.COSFIRE.sigma0               = sigma0 / 6;
params.COSFIRE.alpha                = alpha / 6;
% Inhibition
params.RUSTICO.xi                   = xi;       % Inihbition factor
params.RUSTICO.lambda               = lambda;   % Standard deviation factor

% Orientations
params.invariance.rotation.psilist = 0:pi/numoriens:pi-pi/numoriens;

% Configuration
filterset = configureRUSTICO(line1, round([y x]), params);


%% Application of RUSTICO 
% Pad input image to avoid border effects
NP = 0; 
I = padarray(I, [NP NP], 'replicate');

% Filter response
tuple = computeTuples(I, filterset);
[response, rotations] = applyRUSTICO(I, filterset, params.RUSTICO.xi, tuple);
response = response{1};
response = response(NP+1:end-NP, NP+1:end-NP);
% Save the orientation responses (use of the orientation map)
rotations_final = zeros(size(response, 1), size(response, 2), size(rotations, 3));
for j = 1:size(rotations, 3)
    rotations_final(:, :, j) = rotations(NP+1:end-NP, NP+1:end-NP, j);
end

% Evaluation
perf_threshold = 49; % final threshold on Crack_PV14 data set (corresponding to 0.49 in [0,1])
thresholds = 0.01:0.01:0.99;
figure;
%subplot(1,2,1); imagesc(response); colormap gray; axis off; axis image;
for j = 1:numel(thresholds)
    binImg = binarize(rotations_final, thresholds(j));
    binImg2 = bwmorph(binImg, 'close');
    %subplot(1,2,2); 
    imshow(binImg2);
    drawnow;
    res = evalperf_1pxline(binImg2, GT, 0, 2);
    RESULTS(j, :) = [res.Precision, res.Recall, res.FMeasure];
    binImg = [];
    binImg2 = [];
end

figure;
plot(RESULTS(:, 1), RESULTS(:, 2), 'linewidth', 2, 'color', [0.25 0.25 0.25]);
title('Pr-Re curve');
disp(['pr: ', num2str(RESULTS(49, 1)), ', re: ', num2str(RESULTS(49, 2)), ', F: ', num2str(RESULTS(49, 1))]);
