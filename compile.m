function [ ] = compile( )
% RUSTICO (by Nicola Strisciuglio)
% Compile utility MEX functions before using the operator.

disp('Compiling...');
cd src/RUSTICO
mex dilateDisc.c -output dilate
cd ../..
disp('Compile done.');