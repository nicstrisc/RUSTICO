function [CAL, C, A, L] = CAL_evaluate(resp, gt, mask, SG, gtdil, gtthin)
    % See paper 'A Function for Quality Evaluation of Retinal Vessel Segmentations'
    resp = resp .* mask;
    idx = 1;
    for i = 0:255 %255
        binoutput = resp > i;
        bw = bwlabel(binoutput);
        stats = regionprops(bw);

        nc = numel(stats);
        C(idx) = 1 - min(1,(abs(SG-nc)/sum(gt(:))));

        respdil = imdilate(binoutput,strel('disk',2));

        item1 = gt == respdil & gtdil == 1 & respdil == 1;
        item2 = gtdil == binoutput & gtdil == 1 & binoutput == 1;
        A(idx) = sum(sum(item1 | item2))/sum(sum(gt | binoutput));

        respthin = bwmorph(binoutput, 'thin', inf);

        L(idx) = sum(sum((respthin & gtdil) | (respdil & gtthin)))/sum(sum(respthin | gtthin));

        CAL(idx) = C(idx) * A(idx) * L(idx);
    %     if idx > 1 && CAL(idx) < CAL(idx-1)
    %         break;
    %     end
        idx = idx + 1;
    end