function [cpt, crt, F, pr, re, Fm] = evaluate(binImg, gt, d) % THis is good but we used the standard implementation from Nicolai's student

    A = zeros(d*2+1, d*2+1); A(d+1,d+1) = 1; B = bwdist(A) <= d;
    
    B = ones(d*2+1, d*2+1);
    
    fp = 0;
    tp = 0;
    fn = 0;
    tmp = gt .* ~binImg;
    %fn = sum(tmp(:));
    
    
    [m, n] = size(binImg);
    %binImg = bwmorph(binImg,'skel',Inf);
    gt = padarray(gt, [d d], 0);
    %binImg = padarray(binImg, [d d], 0);
    Lr = 0;

    binImgPad = padarray(binImg, [d d], 0);
    
    
    bad = zeros(size(binImg));
    for x = 1:m
        for y = 1:n
            %if gt(x, y) == 1 
            if binImg(x,y) == 1
                %patch = binImg(x:x+2*d, y:y+2*d); %
                patch = gt(x+d-d:x+d+d, y-d+d:y+d+d);
                s = sum(patch(:) .* B(:));
                if s > 0
                    Lr = Lr + 1;
                    tp = tp + 1;
                else
                    fp = fp + 1;
                    bad(x,y) = 1;
                end
            end
            
            if tmp(x,y) == 1
                %patch = binImg(x:x+2*d, y:y+2*d); %
                patch = binImgPad(x:x+2*d, y:y+2*d);
                s = sum(patch(:) .* B(:));
                if s == 0
                   fn = fn + 1;
                end
            end
        end
    end
    
    pr = tp / (tp + fp);
    re = tp / (tp + fn);
    
    Fm = 2 * pr * re / (pr + re);
    Lgt = sum(gt(:));
    Ln = sum(binImg(:));
    
    cpt = min(1, Lr / Lgt);
    crt = min(1, Lr / Ln);
    F = 2 * cpt * crt / (cpt + crt);
    if crt == 0 && cpt == 0
        F = 0;
    end