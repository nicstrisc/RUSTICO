addpath('../../src/RUSTICO');
addpath('../../src/utils');

% Data set 
% 0: Roses-8, 1: TB-roses-v1, 2: TB-roses-v2
dataset = 1;

% RUSTICO Parameters
rho = 16;
sigma = 2.5;
sigma0 = 3;
alpha = 0.1;
xi = 1.5;         
lambda = 0.5;

% Run with a fixed threhshold -> results published in [1]
threshold = 0.2700; % in [0, 1]
final_results = roses_processing('../../data/TB-roses-v1/', dataset, sigma, rho, sigma0, alpha, lambda, xi, threshold);
mean_results = mean(final_results);
disp(['RUSTICO - Pr: ' num2str(mean_results(1)) ', Re: ' num2str(mean_results(2)) ', F: ' num2str(mean_results(3))]);

% Run experiment (with all the threshold - to draw the Pr-Re curve)
[final_results, avg_results, all_results]  = roses_processing('../../data/TB-roses-v1/', dataset, sigma, rho, sigma0, alpha, lambda, xi);
mean_results = mean(final_results);
disp(['Mean results - Pr: ' num2str(mean_results(1)) ', Re: ' num2str(mean_results(2)) ', F: ' num2str(mean_results(3))]);
figure;
plot(avg_results(:, 2), avg_results(:, 1), 'linewidth', 2, 'color', [0.25 0.25 0.25]);
title('Pr-Re curve - RUSTICO on TB-roses-v1');
ylabel('Pr');
xlabel('Re');

%% Functions to support the experiment
function [ final_results, avg_results, all_results] = roses_processing( datasetpath, dataset, sigma, rho, sigma0, alpha, lambda, xi, thresh )
    TB_ROSES_0 = 0;
    TB_ROSES_V1 = 1;
    TB_ROSES_V2 = 2;

    % Binarization thresholds
    thresholds = 0.01:0.01:0.99;
    nthresholds = numel(thresholds);
    
    if nargin < 6
        error('Insufficient number of parameters.')
    end
    
    if nargin < 8
        lambda = 0;
        xi = 0;        
    end
    
    if nargin == 9 % evaluate the whole set of threholds
        nthresholds = 1;
        thresholds = thresh;
    end

    if dataset == TB_ROSES_0
        dname = 'rose_one';
        imagesdir = 'images';
        gtdir = 'gt';
        prefix_gt = '';
        ext = 'jpg';

    elseif dataset == TB_ROSES_V1
        imagesdir = 'images';        
        gtdir = 'gt';
        prefix_gt = '';
        suffix_gt = '_gt';
        ext = 'jpg';
    elseif dataset == TB_ROSES_V2
        error('TB_ROSES_V2 to be implemented.')
    end
        

    %% Configuration of RUSTICO
    x = 101; y = 101; % center
    line1(:, :) = zeros(201);
    line1(:, x) = 1; %prototype line

    params = config_params;

    % Basic COSFIRE parameters
    params.inputfilter.DoG.sigmalist    = sigma;
    params.COSFIRE.rholist              = 0:2:rho;
    params.COSFIRE.sigma0               = sigma0 / 6;
    params.COSFIRE.alpha                = alpha / 6;
    % Inhibition
    params.RUSTICO.xi                   = xi;       % Inihbition factor
    params.RUSTICO.lambda               = lambda;   % Standard deviation factor

    % Orientations
    numoriens = 8;
    params.invariance.rotation.psilist = 0:pi/numoriens:pi-pi/numoriens;

    % Configuration
    filterset = configureRUSTICO(line1, round([y x]), params);
    
    %% Run experiment
    
    
    % Read files
    files = rdir([datasetpath '/' imagesdir  '/*.' ext]);
    nfiles = size(files, 1);
        
    % Initialize result matrix
    nmetrics = 3; % pr, re, F  
    RESULTS = zeros(nfiles, nmetrics, nthresholds);
        
    for n = 1:nfiles
        disp(['Processing ' num2str(n) ' of ' num2str(nfiles)]);

        [p name e] = fileparts(files(n).name);
        imageInput = double(imread(files(n).name)) ./ 255;
        imageInput = rgb2gray(imageInput);
        [p name ext] = fileparts(files(n).name);
        gt = double(imread([datasetpath gtdir '/' prefix_gt name suffix_gt '.png'])) ./ 255;
        %gt = imread([datasetpath gtdir '/' prefix_gt name suffix_gt '.png']);
        if size(gt, 3) > 1
            gt = gt(:, :, 1);
            %gt = gt > 0.5;
        end
        imageInput = imcomplement(imageInput);

        % Pad input image to avoid border effects
        NP = 50; 
        imageInput = padarray(imageInput, [NP NP], 'replicate');

        %% Filter response
        tuple = computeTuples(imageInput, filterset);
        [response, rotations] = applyRUSTICO(imageInput, filterset, xi, tuple);
        response = response{1};
        response = response(NP+1:end-NP, NP+1:end-NP);

        %% Evaluation of centerline
        rotations_final = zeros(size(response, 1), size(response, 2), size(rotations, 3));
        for j = 1:size(rotations, 3)
            rotations_final(:,:,j) = rotations(NP+1:end-NP, NP+1:end-NP, j);
        end
        gt_line = bwmorph(gt, 'skel', Inf);

        % Evaluation
        d = 3; % evaluation tolerance
        for j = 1:nthresholds
            binImg = binarize(rotations_final, thresholds(j));
            binImg2 = bwmorph(binImg, 'close');
            % Results with tolereance d=3 (px)
            res = evalperf_1pxline(binImg2, gt_line, 0, d);
            pr = res.Precision;
            re = res.Recall;
            Fm = res.FMeasure;
            % Store results for j-th threshold on n-th image of the dataset
            RESULTS(n, :, j) = [pr, re, Fm];
            binImg = [];
            binImg2 = [];
        end

        rotations_final = [];
        rotations = [];
        inputImage = [];
        response = [];
    end

    RESULTS(isnan(RESULTS)) = 0; % Eliminate eventual NaN values due to Sp = Se = 0
    
    if nargin == 1 % retuls for a single threshold (final experiment)
        final_results = RESULTS;
    else
        % results of a complete experiments
        % for different thresholds -> also for computation of the ROC curve
        all_results = RESULTS;
        % Average Results
        avg_results = reshape(mean(RESULTS(1:nfiles, :, :)), nmetrics, nthresholds)';
        %std_results = reshape(std(RESULTS(1:nfiles, :, :)), nmetrics, nthresholds)';
        
        [f, id_f] = max(avg_results(:, 3));
        final_results = RESULTS(:, :, id_f); % per-image results (best threshold)
    end
end
    
    % Export Images
%     if ~isdeployed()
%         return;
%         rotations = rdir([outputdir '/*_rotation.mat']);
%         nfiles = numel(rotations);
% 
%         gts = rdir([datasetpath dname '/' gtdir '/*.png']);
%         
%         load([outputdir '.mat']);
%         avg_results = segres.centerline;
%         for n = 1:nfiles
%             if dataset == ROSE_ONE
%                 [f3 t] = max(avg_results(:,6));
%                 s = 3;
%             elseif dataset == ROSE_TWO
%                 [f3 t] = max(avg_results(:,6));
%                 s = 3;
%             end
%             t = 36;
%             load(rotations(n).name);
%             binImg = binarize(rotations_final, thresholds(t));
%             binImg2 = bwmorph(binImg, 'close');
%             Load Gt
%             gt = double(imread(gts(n).name)) ./ 255;
%             gt = gt(:, :, 1);
%             gt = gt > 0.5;
%             Compute results
%             res = evalperformance(binImg2, gt, 0, s);
%             [f name e] = fileparts(gts(n).name);
%             imwrite(binImg2, [outputdir '/' name '.png']);
%         end
%     end