# Experiments
This section includes the code to replicate the experiment results reported in:

N. Strisciuglio, G. Azzopardi, N. Petkov, __Robust Inhibition-augmented Operator for Delineation of Curvilinear Structures__, _IEEE Transactions on Image Processing, 2019_


## Parameters
Tha parameters used for the experiments are in the following:



